# Shopping Cart #

## What it does ##
This program is a shopping cart simulation that will simulator a customer at checkout.
The program takes into consideration four types of customers.
1. Customer 1: Is not a member, is not tax exempt
2. Customer 2: Is a member, is not tax exempt
3. Customer 3: Is not a member, is tax exempt
4. Customer 4: Is a member, is tax exempt

For our database, we used a HashMap to have a Key-Value pair of items with the proper Item object.
These are created for each individual test. That is, a shopping cart will only have items added to it if a customer adds the item.
The most important method to this program is the calcPurchasePrice method. This method calculates the proper purchase price by
taking into consideration how many items the shopping cart has, if there is any discount for number of items in a cart, is the customer is a member
and if the customer is tax exempt.

## Tests ##
Several tests where created to test the following classes: Item, ShoppingCart, and Customer. These were the three primary classes needed to complete the program.
The most intensive of the test cases were that of the ShoppingCart. Since the shopping cart does majority of the logic we needed to test several cases.
This included special cases, such as having 10 or 50 items for a discount, and trivial cases that are in between the boundaries of a special case (1-4 items, 10-50 items, etc).


# Developer Log #
March 15 - Created basic classes like Customer, Item, shopping cart etc. Item list needs to be implemented, not sure if we can use a database.

March 16 - Got rid of extra class Item_List which was not needed.

March 20 - Major refactor from initial code. Found out that we may not need all classes. Started to use a HashMap instead of an ArrayList
to hold references to Item objects. Began to write tests and found some bugs, this included having to create a method for calculating discounts.

## Instructions for running tests ##
1. You can import the repo(project) to your local machine using the link below:
https://Ayyub_Patel@bitbucket.org/Ayyub_Patel/shoppingcart_java.git
You can use 'git clone' command in terminal (mac) to import.

2. Open the project in IntelliJ Idea (IDE). You can also use other IDEs like Eclipse but for this project we are using IntelliJ.

3. Once the project is open, you can see all the folders in the left pane when you expand the project folder (shoppingcart_java) itself.

4. All the classes are in 'src' folder, expand it and open different classes to familiarize yourself with the code. Similarly go to 'out' --> 'test' --> 'cart', to see different test cases we have covered.

5. To run all the tests, right click on 'src' in IntelliJ Idea, and then choose 'Run All Tests'. Or, just select or highlight 'src' folder then press ctrl+shift+R (for mac).