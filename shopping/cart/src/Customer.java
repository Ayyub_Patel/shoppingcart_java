public class Customer {
	int cId;
	String cName;
	Boolean isMember;
	Boolean isTaxExempt;
	
	public Customer() {}
	
	public Customer(int cId, String cName, Boolean isMember, Boolean isTaxExempt) {
		super();
		this.cId = cId;
		this.cName = cName;
		this.isMember = isMember;
		this.isTaxExempt = isTaxExempt;
	}

	public int getcId() {
		return cId;
	}

	public void setcId(int cId) {
		this.cId = cId;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public Boolean getIsMember() {
		return isMember;
	}

	public void setIsMember(Boolean isMember) {
		this.isMember = isMember;
	}

	public Boolean getIsTaxExempt() { return isTaxExempt; }

	public void setIsTaxExempt(Boolean isTaxExempt) { this.isTaxExempt = isTaxExempt; }

}
