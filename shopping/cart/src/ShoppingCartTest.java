import junit.framework.TestCase;
import org.junit.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by tylerbobella on 3/20/16.
 */
public class ShoppingCartTest extends TestCase {

    ShoppingCart cart;
    Customer cust1, cust2, cust3, cust4;
    private static final double DELTA = 1e-15;
    List<Item> zeroItems = new ArrayList<Item>();
    List<Item> fourItems = new ArrayList<Item>();
    List<Item> fiveItems = new ArrayList<Item>();
    List<Item> sevenItems = new ArrayList<Item>();
    List<Item> tenItems = new ArrayList<Item>();
    List<Item> fiftyItems = new ArrayList<Item>();
    List<Item> fiftyOneItems = new ArrayList<Item>();

    @Before
    public void setUp() throws Exception {
        cart = new ShoppingCart();
        cust1 = new Customer(1, "Test1", false, false);
        cust2 = new Customer(2, "Test2", true, false);
        cust3 = new Customer(3, "Test3", false, true);
        cust4 = new Customer(4, "Test4", true, true);

        double price = 2.00;
        // Four items
        for (int i = 1; i <= 4; i++) {
            Item item = new Item(String.valueOf(i), "item" + i, price);
            fourItems.add(item);
        }

        // Five items
        for (int i = 1; i <= 5; i++) {
            Item item = new Item(String.valueOf(i), "item" + i, price);
            fiveItems.add(item);
        }

        // Seven items
        for (int i = 1; i <= 7; i++) {
            Item item = new Item(String.valueOf(i), "item" + i, price);
            sevenItems.add(item);
        }

        // Ten items
        for (int i = 1; i <= 10; i++) {
            Item item = new Item(String.valueOf(i), "item" + i, price);
            tenItems.add(item);
        }

        // Fifty items
        for (int i = 1; i <= 50; i++) {
            Item item = new Item(String.valueOf(i), "item" + i, price);
            fiftyItems.add(item);
        }

        // Fifty-one items
        for (int i = 1; i <= 51; i++) {
            Item item = new Item(String.valueOf(i), "item" + i, price);
            fiftyOneItems.add(item);
        }
    }
@Test
    public void testEmptyCart() {
        for (Item item : zeroItems) {
            cart.addItem(item);
        }
        Assert.assertTrue(cart.getNumberOfItems() == 0);
    }

    @Test
    public void testAddItemToCart() {
        for (Item item : fourItems) {
            cart.addItem(item);
        }
        Assert.assertTrue(cart.getNumberOfItems() == 4);
    }

    /*
    With customer 1
    Testing all cases with a normal customer
     */
    @Test
    public void testCust1AndFourItems() {
        // Set up list of keys and add items to Map
        // In our shopping card
        List<String> keys = new ArrayList<String>();
        for (Item item : fourItems) {
            cart.addItem(item);
            keys.add(item.getId());
        }

        double [] prices = cart.calcPurchasePrice(keys, cust1);
        assertEquals(prices[0], 8.0, DELTA);
        assertEquals(prices[1], 0.0, DELTA);
        assertEquals(prices[2], 8.0, DELTA);
    }


    @Test
    public void testCust1AndFiveItems() {
        List<String> keys = new ArrayList<String>();
        for (Item item : fiveItems) {
            cart.addItem(item);
            keys.add(item.getId());
        }

        double [] prices = cart.calcPurchasePrice(keys, cust1);
        assertEquals(prices[0], 10.0, DELTA);
        assertEquals(prices[1], 1.0, DELTA);
        assertEquals(prices[2], 9.0, DELTA);
    }

    @Test
    public void testCust1AndSevenItems() {
        List<String> keys = new ArrayList<String>();
        for (Item item : sevenItems) {
            cart.addItem(item);
            keys.add(item.getId());
        }

        double [] prices = cart.calcPurchasePrice(keys, cust1);
        assertEquals(prices[0], 14.0, DELTA);
        assertEquals(prices[1], 1.0, DELTA);
        assertEquals(prices[2], 14.0, DELTA);
    }

    @Test
    public void testCust1AndTenItems() {
        List<String> keys = new ArrayList<String>();
        for (Item item : tenItems) {
            cart.addItem(item);
            keys.add(item.getId());
        }

        double [] prices = cart.calcPurchasePrice(keys, cust1);
        assertEquals(prices[0], 20.0, DELTA);
        assertEquals(prices[1], 2.0, DELTA);
        assertEquals(prices[2], 19.0, DELTA);
    }

    @Test
    public void testCust1AndFiftyItems() {
        List<String> keys = new ArrayList<String>();
        for (Item item : fiftyItems) {
            cart.addItem(item);
            keys.add(item.getId());
        }

        double [] prices = cart.calcPurchasePrice(keys, cust1);
        assertEquals(prices[0], 100.0, DELTA);
        assertEquals(prices[1], 10.0, DELTA);
        assertEquals(prices[2], 94.0, DELTA);
    }

    @Test
    public void testCust1AndFiftyOneItems() {
        List<String> keys = new ArrayList<String>();
        for (Item item : fiftyOneItems) {
            cart.addItem(item);
            keys.add(item.getId());
        }

        double [] prices = cart.calcPurchasePrice(keys, cust1);
        assertNull(prices);
    }

    /*
    Customer 2
    Testing first & second case with membership discount
     */

    @Test
    public void testCust2AndFourItems() {
        List<String> keys = new ArrayList<String>();
        for (Item item : fourItems) {
            cart.addItem(item);
            keys.add(item.getId());
        }

        double [] prices = cart.calcPurchasePrice(keys, cust2);
        assertEquals(prices[0], 8.0, DELTA);
        assertEquals(prices[1], 1.0, DELTA);
        assertEquals(prices[2], 7.0, DELTA);
    }

    @Test
    public void testCust2AndFiveItems() {
        List<String> keys = new ArrayList<String>();
        for (Item item : fiveItems) {
            cart.addItem(item);
            keys.add(item.getId());
        }

        double [] prices = cart.calcPurchasePrice(keys, cust2);
        assertEquals(prices[0], 10.0, DELTA);
        assertEquals(prices[1], 2.0, DELTA);
        assertEquals(prices[2], 8.0, DELTA);
    }

    /*
    Customer 3
    Testing first and second case with tax exempt
     */
    @Test
    public void testCust3AndFourItems() {
        List<String> keys = new ArrayList<String>();
        for (Item item : fourItems) {
            cart.addItem(item);
            keys.add(item.getId());
        }

        double [] prices = cart.calcPurchasePrice(keys, cust3);
        assertEquals(prices[0], 8.0, DELTA);
        assertEquals(prices[1], 0.0, DELTA);
        assertEquals(prices[2], 8.0, DELTA);
    }

    @Test
    public void testCust3AndFiveItems() {
        List<String> keys = new ArrayList<String>();
        for (Item item : fiveItems) {
            cart.addItem(item);
            keys.add(item.getId());
        }

        double [] prices = cart.calcPurchasePrice(keys, cust3);
        assertEquals(prices[0], 10.0, DELTA);
        assertEquals(prices[1], 1.0, DELTA);
        assertEquals(prices[2], 9.0, DELTA);
    }

    /*
    Customer 4
    Test first and second case with membership discount and tax exempt
     */
    @Test
    public void testCust4AndFourItems() {
        List<String> keys = new ArrayList<String>();
        for (Item item : fourItems) {
            cart.addItem(item);
            keys.add(item.getId());
        }

        double [] prices = cart.calcPurchasePrice(keys, cust4);
        assertEquals(prices[0], 8.0, DELTA);
        assertEquals(prices[1], 1.0, DELTA);
        assertEquals(prices[2], 7.0, DELTA);
    }

    @Test
    public void testCust4AndFiveItems() {
        List<String> keys = new ArrayList<String>();
        for (Item item : fiveItems) {
            cart.addItem(item);
            keys.add(item.getId());
        }

        double [] prices = cart.calcPurchasePrice(keys, cust4);
        assertEquals(prices[0], 10.0, DELTA);
        assertEquals(prices[1], 2.0, DELTA);
        assertEquals(prices[2], 8.0, DELTA);
    }
}