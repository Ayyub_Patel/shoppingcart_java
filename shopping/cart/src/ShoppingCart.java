import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingCart {
	Map<String, Item> items;
	int numberOfItems;
	double total;
    protected final double salesTaxRate = 0.045;
    double taxAmount = 0.0;
 
    public ShoppingCart() {
        items = new HashMap<String, Item>();
        numberOfItems = 0;
        total = 0;
    }
 
    public void addItem(Item item) {
        items.put(item.getId(), item);
    }

    public double[] calcPurchasePrice(List<String> productIds, Customer customer) {
        String check = checkProductListValidity(productIds, customer);
        double basePrice = 0.00;
        double discountPrice = 0.00;

        if (check != null) {
            System.out.println(check);
            return null;
        }


        for (Map.Entry<String, Item> entry : items.entrySet()) {
            basePrice += entry.getValue().getPrice();
        }

        if (items.size() >= 5 && items.size() < 10) {
            discountPrice += 0.05 * basePrice;
        }

        if (items.size() >= 10 && items.size() <= 50) {
            discountPrice += 0.10 * basePrice;
        }

        if (customer.getIsMember()) {
            discountPrice += 0.10 * basePrice;
        }

        basePrice = Math.round(basePrice);
        discountPrice = Math.round(discountPrice);

        if (customer.getIsTaxExempt()) {
            double [] finalPrices = { Math.round(basePrice), Math.round(discountPrice), Math.round((basePrice - discountPrice)) };
            return finalPrices;
        }

        taxAmount = (basePrice - discountPrice) * salesTaxRate;
        double finalPrice = taxAmount + (basePrice - discountPrice);
        double [] finalPrices = { Math.round(basePrice), Math.round(discountPrice), Math.round(finalPrice) };
        return finalPrices;

    }

    public String checkProductListValidity(List<String> productIds, Customer customer) {

        if (productIds.size() > 50) {
            return "There cannot be more than 50 items";
        }

        if (productIds.size() <= 0) {
            return "The shopping cart cannot have zero or negative values";
        }

        if (customer == null) {
            return "Invalid Customer. Customer must be present";
        }
        return null;
    }

    public List<Item> getItems() {
        List<Item> cartItems = new ArrayList<Item>();
        for (Map.Entry<String, Item> item : items.entrySet()) {
            cartItems.add(item.getValue());
        }
        return cartItems;
    }

    public Item getItem(String itemId) {
        for (Map.Entry<String, Item> item : items.entrySet()) {
            if (itemId == item.getValue().getId()) {
              return item.getValue();
            }
        }
        return null;
    }
 
    public synchronized int getNumberOfItems() {
 
        return items.size();
    }

    public synchronized void clear() {
        items.clear();
        numberOfItems = 0;
        total = 0;
    }
 
}
