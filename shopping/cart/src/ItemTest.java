import junit.framework.Assert;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by tylerbobella on 3/20/16.
 */
public class ItemTest extends TestCase {
    Item item1;
    Item item2;
    Item item3;
    Item item4;
    Item item5;

    @Before
    public void setUp() throws Exception {
        item1 = new Item("1", "Item 1", 2.99);
        item2 = new Item("2", "Item 2", 0.99);
        item3 = new Item("3", "Item 3", 1);
        item4 = new Item("4", "Item 4", 2.99);
        item5 = new Item("4", "Item 4", 2.99);
    }

    @Test
    public void testItemValue() {
        assertTrue(item1.getPrice() == 2.99);
        assertTrue(item2.getPrice() != 1);
        assertTrue(item3.getPrice() == 1);
        assertTrue(item4.getPrice() != 2.98);
    }

    @Test
    public void testIfItemAreEqual() {
//        assertTrue(item5.equals(item4));
//        assertTrue(item4.equals(item5));

        assertFalse(item1.equals(item5));
        assertFalse(item2.equals(item3));
        assertFalse(item3.equals(item4));
    }
}
