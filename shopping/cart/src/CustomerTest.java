import junit.framework.TestCase;
import org.junit.*;

/**
 * Created by tylerbobella on 3/17/16.
 */
public class CustomerTest extends TestCase {
    Customer cust1;
    Customer cust2;
    Customer cust3;
    Customer cust4;

    @Before
    public void setUp() throws Exception {
        cust1 = new Customer(1, "Test1", false, false);
        cust2 = new Customer(2, "Test2", true, false);
        cust3 = new Customer(3, "Test3", false, true);
        cust4 = new Customer(4, "Test4", true, true);
    }

    @Test
    public void testMembership() {
        assertFalse("Customer 1 is not a member", cust1.getIsMember());
        assertFalse("Customer 3 is not a member", cust3.getIsMember());

        assertTrue("Customer 2 is a member", cust2.getIsMember());
        assertTrue("Customer 4 is a member", cust4.getIsMember());

    }

    @Test
    public void testTaxExempt() {
        assertFalse("Customer 1 is not exempt", cust1.getIsTaxExempt());
        assertFalse("Customer 2 is exempt", cust2.getIsTaxExempt());

        assertTrue("Customer 3 is not exempt", cust3.getIsTaxExempt());
        assertTrue("Customer 4 is exempt", cust4.getIsTaxExempt());
    }

}